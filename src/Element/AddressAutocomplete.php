<?php

declare(strict_types=1);

namespace Drupal\address_autocomplete_photon\Element;

use CommerceGuys\Addressing\AddressFormat\AddressFormatHelper;
use CommerceGuys\Addressing\Locale;
use Drupal\address\Element\Address;
use Drupal\address\FieldHelper;
use Drupal\address\Plugin\Field\FieldFormatter\AddressDefaultFormatter;

/**
 * Provides an address autocomplete form element. Extend address form elemnt.
 *
 * Refer to address form element to read fully documentation.
 *
 * @see \Drupal\address\Element\Address
 *
 * Usage example:
 * @code
 * $form['address'] = [
 *   '#type' => 'address_autocomplete',
 *   '#default_value' => [
 *     'given_name' => 'John',
 *     'family_name' => 'Smith',
 *     'organization' => 'Google Inc.',
 *     'address_line1' => '1098 Alta Ave',
 *     'postal_code' => '94043',
 *     'locality' => 'Mountain View',
 *     'administrative_area' => 'CA',
 *     'country_code' => 'US',
 *     'langcode' => 'en',
 *   ],
 *   '#field_overrides' => [
 *     AddressField::ORGANIZATION => FieldOverride::REQUIRED,
 *     AddressField::ADDRESS_LINE2 => FieldOverride::HIDDEN,
 *     AddressField::POSTAL_CODE => FieldOverride::OPTIONAL,
 *   ],
 *   '#allow_overrides' => FALSE,
 *   '#available_countries' => ['DE', 'FR'],
 * ];
 * @endcode
 *
 * @FormElement("address_autocomplete")
 */
class AddressAutocomplete extends Address {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return parent::getInfo() + [
      // Add toggle button to allow user to overrides autocompleted values.
      '#allow_overrides' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected static function addressElements(array $element, array $value): array {
    $element = parent::addressElements($element, $value);
    $default = static::prepareDefault($element, $value);

    $allowOverrides = (
      $element['#allow_overrides'] &&
      \Drupal::currentUser()->hasPermission('override address fields')
    );

    $settings =
      \Drupal::config('address_autocomplete_photon.settings')
        ->get('autocomplete') + [
          'allow_overrides' => $allowOverrides,
          'default_country' => $value['country_code'],
          'format' => '',
        ];

    // Set element required if at least one field is required.
    if (!empty($value['country_code'])) {
      /** @var \CommerceGuys\Addressing\AddressFormat\AddressFormat $addressFormat */
      $addressFormat = \Drupal::service('address.address_format_repository')
        ->get($value['country_code']);

      $requiredFields = AddressFormatHelper::getRequiredFields(
        $addressFormat,
        $element['#parsed_field_overrides']
      );

      $settings['format'] = str_replace("\n", ' ',
        self::getAddressFormat($value['country_code'])
      );
    }

    $element['location_field'] = [
      '#type' => 'textfield',
      '#title' => t('Address (with autocompletion)'),
      '#default_value' => $default,
      '#required' => isset($requiredFields) && !empty($requiredFields),
      '#attributes' => [
        'class' => [
          'address-autocomplete-input',
        ],
      ],
      '#maxlength' => 2048,
      '#weight' => -99,
    ];

    $element['#attributes']['class'][] = 'address-autocomplete-wrapper';
    $element['#attached']['library'][] = 'address_autocomplete_photon/autocomplete';
    $element['#attached']['drupalSettings']['addressAutocomplete'] = $settings;

    return $element;
  }

  /**
   * Get address format.
   *
   * @param string $countryCode
   *   The current country code.
   *
   * @return string
   *   The address format.
   */
  private static function getAddressFormat(string $countryCode): string {
    /** @var \CommerceGuys\Addressing\AddressFormat\AddressFormat $addressFormat */
    $addressFormat = \Drupal::service('address.address_format_repository')
      ->get($countryCode);
    $locale = \Drupal::languageManager()->getConfigOverrideLanguage()->getId();

    if (Locale::matchCandidates($addressFormat->getLocale(), $locale)) {
      return $addressFormat->getLocalFormat();
    }

    return $addressFormat->getFormat();
  }

  /**
   * Prepare element default value.
   *
   * @param array $element
   *   The form element.
   * @param array $value
   *   The element value.
   *
   * @return string
   *   The element default value.
   */
  protected static function prepareDefault(array $element, array $value): string {
    if (empty($value) || empty($value['country_code'])) {
      return '';
    }

    /** @var \CommerceGuys\Addressing\AddressFormat\AddressFormat $addressFormat */
    $addressFormat = \Drupal::service('address.address_format_repository')
      ->get($value['country_code']);

    $replacements = [];
    foreach ($addressFormat->getUsedFields() as $field) {
      $property = FieldHelper::getPropertyName($field);
      $replacements['%' . $field] = $value[$property] ?? '';
    }

    $default = AddressDefaultFormatter::replacePlaceholders(
      self::getAddressFormat($value['country_code']),
      $replacements
    );
    return trim(str_replace("\n", ' ', $default));
  }

}
