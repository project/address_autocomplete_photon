<?php

declare(strict_types=1);

namespace Drupal\address_autocomplete_photon\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;

/**
 * Plugin implementation of the 'address_autocomplete_photon' widget.
 *
 * @FieldWidget(
 *   id = "address_autocomplete_photon",
 *   label = @Translation("Address autocomplete with Photon"),
 *   field_types = {
 *     "address"
 *   }
 * )
 */
class AddressAutocomplete extends AddressDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return parent::defaultSettings() + [
      'allow_overrides' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['address']['#type'] = 'address_autocomplete';
    $element['address']['#allow_overrides'] = (bool) $this->getSetting('allow_overrides');

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['allow_overrides'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow users to override autocompleted values.'),
      '#description' => $this->t('Bypass global setting "Managed fields display" to allow user to override autocompleted values.'),
      '#default_value' => $this->getSetting('allow_overrides'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Allow overrides: @value', [
      '@value' => $this->getSetting('allow_overrides') ? $this->t('True') : $this->t('False'),
    ]);

    return $summary;
  }

}
