/**
 * @file
 * Address autocomplete for Photon API plugin.
 */

(($, Drupal) => {
  "use strict";

  // Parameters.
  const pluginName = "addressAutocompletePhoton";
  const defaults = {
    allowOverrides: false,
    defaultCountry: "US",
    format: "%givenName %familyName %organization %addressLine1 %addressLine2 %addressLine3 %locality",
    lang: $("html").attr("lang"),
    limit: 3,
    minLength: 1,
    mode: "hide",
    removeDuplicates: true,
    toggleButton: ".address-autocomplete-toggle-button",
    wrapper: ".address-autocomplete-wrapper"
  };

  // Plugin constructor.
  const Plugin = function (element, options, i) {
    // Plugin exposed to window.
    window[pluginName + i] = this;

    // Options
    this.settings = $.extend({}, defaults, options);

    // Address managed fields list.
    this.addressFields = {
      organization: ".organization",
      addressLine1: ".address-line1",
      addressLine2: ".address-line2",
      addressLine3: ".address-line3",
      dependentLocality: ".sublocality-level1",
      postalCode: ".postal-code",
      locality: ".locality",
      state: ".administrative-area",
      administrativeArea: ".state",
      country: ".country"
    };

    // DOM elements.
    this.$input = $(element).attr(`data-${pluginName}-id`, pluginName + i);
    this.$wrapper = this.$input.closest(this.settings.wrapper);
    this.$countryField = $(this.addressFields.country, this.$wrapper);
    this.$toggleFieldsButton = false;

    // Toggle fields state.
    this.toggleFieldsIsShown = false;

    // Initialize plugin.
    this.init();
  };

  // Shortcut for Plugin object prototype
  Plugin.fn = Plugin.prototype;

  /**
   * Initialize plugin.
   */
  Plugin.fn.init = function () {
    // Hide/Disable all other address fields.
    this.hideFields();

    // Init overrides toggle button.
    this.initToggleFieldsButton();

    // Initialize autocomplete selection.
    this.autocompleteSelected = false;

    this.events();
  };

  /**
   * Initialize show/hide toggle fields button.
   */
  Plugin.fn.initToggleFieldsButton = function () {
    if (
      !this.settings.allowOverrides ||
      this.settings.mode === "default"
    ) {
      return;
    }

    this.$toggleFieldsButton = $(
      Drupal.theme("addressAutocompleteToggleButton", this.settings.mode)
    );
    this.$input.after(this.$toggleFieldsButton);
  };

  /**
   * Plugin events
   */
  Plugin.fn.events = function () {
    const that = this;

    // Initialize autocomplete.
    this.$input.autocomplete({
      autoFocus: true,
      minLength: this.settings.minLength,
      source(request, response) {
        that.autocompleteSource(request, response);
      },
      change() {
        that.autocompleteCheckValidity();
      },
      select(event, ui) {
        that.autocompleteSelected = true;
        that.$input.get(0).setCustomValidity("");

        that.autocompleteSelect(event, ui);
      }
    });

    if (this.$toggleFieldsButton) {
      this.$toggleFieldsButton.on("click", function (event) {
        that.eventToggleButton(event);
      });
    }
  };

  /**
   * Event management for toggle button.
   *
   * @param {Event} event
   *   The event object.
   */
  Plugin.fn.eventToggleButton = function(event) {
    const that = this;

    // Toggle state.
    this.toggleFieldsIsShown = (this.toggleFieldsIsShown === false);

    const $toggleFieldsButton = $(
      Drupal.theme(
        "addressAutocompleteToggleButton",
        this.settings.mode,
        this.toggleFieldsIsShown
      )
    );

    if (this.toggleFieldsIsShown) {
      this.$input.prop("disabled", true);
      this.showFields();
    }
    else {
      this.$input.prop("disabled", false);
      this.hideFields();
    }

    // Replace button rendering.
    this.$toggleFieldsButton.replaceWith($toggleFieldsButton);
    this.$toggleFieldsButton = $toggleFieldsButton;

    this.$toggleFieldsButton.on("click", function (event) {
      that.eventToggleButton(event);
    });

    event.preventDefault();
  };

  /**
   * jQuery autocomplete check field validity.
   */
  Plugin.fn.autocompleteCheckValidity = function () {
    // In case of field is required and user not select item, add custom validity.
    if (
      this.$input.prop("required") &&
      this.autocompleteSelected === false
    ) {
      this.$input
        .get(0)
        .setCustomValidity(Drupal.t("You must to select a suggested address."));
      this.$input.get(0).reportValidity();
    }
  };

  /**
   * jQuery autocomplete select event.
   *
   * @see https://api.jqueryui.com/autocomplete/#event-select
   *
   * @param {Event} event
   *   The change event.
   * @param {Object} ui
   *   The selected item.
   */
  Plugin.fn.autocompleteSelect = function (event, ui) {
    if (typeof ui.item.result.properties.type === "undefined") {
      return;
    }

    const {properties} = ui.item.result;
    const responseMapping = this.getResponseMapping(properties);

    for (const [component, addressField] of Object.entries(
      this.addressFields
    )) {
      const $componentField = this.$wrapper.find(addressField);
      let value = "";

      if (!$componentField.length) {
        continue;
      }

      if (
        responseMapping.hasOwnProperty(component) &&
        properties.hasOwnProperty(responseMapping[component])
      ) {
        value = properties[responseMapping[component]];
      }

      // Do not try to change country field value if it's the same country.
      // Prevent "Address" form element AJAX callback.
      if (
        component === "country" &&
        value === this.getCountryValue()
      ) {
        continue;
      }

      // The route is a concatenation of street number and street name.
      if (
        component === "addressLine1" &&
        typeof properties.housenumber !== "undefined"
      ) {
        // Format address field according to selected country.
        value = this.formatAddressLine1(properties.housenumber, value);
      }

      $componentField.val(value);

      // In case of field has attribute "readonly", we need to remove it to enable browser "checkValidity".
      this.displayField($componentField);

      // Photon response is empty or component field is invalid, display it (or make it focusable).
      if (!$componentField.get(0).checkValidity()) {
        $componentField.get(0).reportValidity();
      }
      else {
        // Ensure the field is hidden or not focusable.
        this.hideField($componentField);
      }

      // Change event is not triggered when field is disabled or hidden.
      // Trigger it to allow user to listen "change" event.
      if (this.settings.mode !== "default") {
        $componentField.trigger("change");
      }
    }
  };

  /**
   * jQuery autocomplete source option.
   *
   * @see https://api.jqueryui.com/autocomplete/#option-source
   *
   * @param {Object} request
   *   The request object.
   * @param {Function} response
   *   The response function.
   */
  Plugin.fn.autocompleteSource = function (request, response) {
    const that = this;
    const countryValue = this.getCountryValue();
    const countryValueLabel = $(
      `option[value="${countryValue}"]`,
      this.$countryField
    ).text();
    const searchTerm = `${request.term} ${countryValueLabel}`;

    $.getJSON(
      "https://photon.komoot.io/api/",
      {
        lang: this.settings.lang,
        q: searchTerm
      },
      function (data) {
        if (typeof data.features === "undefined") {
          response();
          return;
        }

        const autocompleteResults = [];

        /**
         * @param {int} index
         * @param {PhotonResult} result
         */
        $.each(data.features, function (index, result) {
          // Exclude other countries.
          if (result.properties.countrycode !== countryValue) {
            // This is equivalent of 'continue' for jQuery loop.
            return;
          }

          // In some cases, the Photon API result may contains multiple postal codes.
          that.shiftPostalCodes(result.properties);

          const formattedValue = that.formatProperties(result.properties);

          // The Photon API can generate duplicates for some locations (i.e. cities that are states for example), this option will remove them.
          if (that.settings.removeDuplicates) {
            const existingResults = $.grep(autocompleteResults, function (
              resultItem
            ) {
              return resultItem.value === formattedValue;
            });

            if (existingResults.length === 0) {
              autocompleteResults.push({
                value: formattedValue,
                result
              });
            }
          }
          else {
            autocompleteResults.push({
              value: formattedValue,
              result
            });
          }

          if (autocompleteResults.length >= that.settings.limit) {
            // This is equivalent of 'break' for jQuery loop.
            return false;
          }
        });

        response(autocompleteResults);
      }
    );
  };

  /**
   * Display (or set focusable) given field.
   *
   * @param {jQuery} $componentField
   *   The component field jQuery object.
   */
  Plugin.fn.displayField = function ($componentField) {
    if (this.settings.mode === "hide") {
      this.$wrapper.find(`label[for="${$componentField.attr("id")}"]`).show();
      $componentField.show();
    }
    else if (this.settings.mode === "disable") {
      $componentField.removeAttr("readonly");
    }
  };

  /**
   *  Format item result properties before to display it to user.
   *
   * @param {Object} properties
   *   A Photon item result properties.
   *
   * @return {string}
   *   Properties formatted.
   */
  Plugin.fn.formatProperties = function (properties) {
    const responseMapping = this.getResponseMapping(properties);
    let addressArguments = {
      "%addressLine1": "",
      "%addressLine2": "",
      "%addressLine3": "",
      "%administrativeArea": "",
      "%country": "",
      "%dependentLocality": "",
      "%familyName": "",
      "%givenName": "",
      "%locality": "",
      "%organization": "",
      "%postalCode": "",
      "%sortingCode": ""
    };

    for (const component of Object.keys(this.addressFields)) {
      if (
        responseMapping.hasOwnProperty(component) &&
        properties.hasOwnProperty(responseMapping[component])
      ) {
        let value = properties[responseMapping[component]];

        // The route is a concatenate of street number and street name.
        if (
          component === "addressLine1" &&
          typeof properties.housenumber !== "undefined"
        ) {
          // Format address field according to selected country.
          value = this.formatAddressLine1(properties.housenumber, value);
        }
        else if (component === "country") {
          // Use human-readable name.
          value = properties.country;
        }

        addressArguments["%" + component] = value;
      }
    }

    return $.trim(
      Drupal.stringReplace(
        this.settings.format,
        addressArguments
      ).replace(/\s{2,}/g, " ")
    );
  };

  /**
   * Format address field according to selected country.
   *
   * @param {string} streetNumber
   *   The street number.
   * @param {string} streetName
   *   The street name.
   *
   * @return {string}
   *   The addressLine1 formatted value.
   */
  Plugin.fn.formatAddressLine1 = function (streetNumber, streetName) {
    if (streetNumber === "") {
      return streetName;
    }

    let streetValue = "";
    switch (this.getCountryValue()) {

      case "AU": // Australia
      case "BG": // Bulgaria
      case "CA": // Canada
      case "FR": // France
      case "HK": // Hong Kong
      case "IE": // Ireland
      case "IL": // Israel
      case "IT": // Italy
      case "JP": // Japan
      case "MY": // Malaysia
      case "NZ": // New Zealand
      case "PH": // Philippines
      case "SA": // Saudi Arabia
      case "SG": // Singapore
      case "LK": // Sri Lanka
      case "TH": // Thailand
      case "GB": // United Kingdom
      case "US": // United States
      case "VN": // Vietnam
        streetValue = streetNumber + " " + streetName;
        break;

      case "IN": // India
        streetValue = streetNumber + ", " + streetName;
        break;

      case "CL": // Chile
        streetValue = streetName + " N° " + streetNumber;
        break;

      case "ID": // Indonesia
      case "MX": // Mexico
        streetValue = streetName + " No. " + streetNumber;
        break;

      case "TW": // Taiwan
        streetValue = " No. " + streetNumber + " " + streetName;
        break;

      case "IQ": // Iraq
        streetValue = streetName + " no. " + streetNumber;
        break;

      case "MO": // Macao
        streetValue = streetName + ", n.o " + streetNumber;
        break;

      case "RO": // Romania
        streetValue = streetName + ", nr. " + streetNumber;
        break;

      case "BY": // Belarus
      case "BR": // Brazil
      case "CN": // China
      case "OM": // Oman
      case "PK": // Pakistan
      case "RU": // Russia
      case "SP": // Spain
      case "UA": // Ukraine
        streetValue = streetName + ", " + streetNumber;
        break;

      default:
        streetValue = streetName + " " + streetNumber;
        break;
    }

    return streetValue;
  };

  /**
   * Get the country field value.
   *
   * @return {string}
   *   The current country field value.
   */
  Plugin.fn.getCountryValue = function () {
    const value = this.$countryField.val();
    if (value) {
      return value;
    }

    return this.settings.defaultCountry;
  };

  /**
   *  Mapping between response item properties and address fields.
   *
   * @param {Object} properties
   *   A Photon item result properties.
   *
   * @return {string}
   *   Properties mapped.
   */
  Plugin.fn.getResponseMapping = function (properties) {
    let responseMapping = {
      addressLine1: "name",
      country: "countrycode",
      dependentLocality: "locality",
      locality: "city",
      administrativeArea: "state",
      postalCode: "postcode",
      state: "state"
    };

    // Specific mapping by type of result.
    if (properties.type === "city") {
      responseMapping.addressLine1 = "";
      responseMapping.locality = "name";
    }
    else if (properties.type === "house") {
      responseMapping.organization = "name";
      responseMapping.addressLine1 = "street";
    }

    return responseMapping;
  };

  /**
   * Hide (or set readonly) given field.
   *
   * @param {jQuery} $componentField
   *   The component field jQuery object.
   */
  Plugin.fn.hideField = function ($componentField) {
    if (this.settings.mode === "hide") {
      this.$wrapper.find(`label[for="${$componentField.attr("id")}"]`).hide();
      $componentField.hide();
    }
    else if (this.settings.mode === "disable") {
      $componentField.attr("readonly", "readonly");
    }
  };

  /**
   * Hide (or set readonly) fields.
   */
  Plugin.fn.hideFields = function () {
    for (const [component, componentClass] of Object.entries(
      this.addressFields
    )) {
      if (component === "country") {
        continue;
      }

      this.hideField(this.$wrapper.find(componentClass));
    }
  };

  /**
   * Show (or unset readonly) given field.
   *
   * @param {jQuery} $componentField
   *   The component field jQuery object.
   */
  Plugin.fn.showField = function ($componentField) {
    if (this.settings.mode === "hide") {
      this.$wrapper.find(`label[for="${$componentField.attr("id")}"]`).show();
      $componentField.show();
    }
    else if (this.settings.mode === "disable") {
      $componentField.removeAttr("readonly");
    }
  };

  /**
   * Show (or unset readonly) fields.
   */
  Plugin.fn.showFields = function () {
    for (const [component, componentClass] of Object.entries(
      this.addressFields
    )) {
      if (component === "country") {
        continue;
      }

      this.showField(this.$wrapper.find(componentClass));
    }
  };

  /**
   * Shift "postcode" result item property if there is several postcode.
   *
   * @param {object} properties
   *   Photon item result properties.
   */
  Plugin.fn.shiftPostalCodes = function (properties) {
    if (!properties.hasOwnProperty("postcode")) {
      return;
    }

    const postalCodes = properties.postcode.split(";");
    if (postalCodes.length > 1) {
      properties.postcode = postalCodes[0];
    }
  };

  /**
   * Plugin wrapper around the constructor, preventing against multiple instantiations.
   *
   * @param {Object} options
   *   The plugin options.
   *
   * @return {Array}
   *   The plugin instantiations.
   */
  $.fn[pluginName] = function (options) {
    return this.each(function (i) {
      if (!$.data(this, `plugin_${pluginName}`)) {
        $.data(this, `plugin_${pluginName}`, new Plugin(this, options, i));
      }
    });
  };

  /**
   * A toggle button for displayed fields.
   *
   * @param {string} mode
   *   The used display mode.
   * @param {boolan} [isShown]
   *   Fields are shown or not.
   * @return {string}
   *   The HTML markup for the toggle button.
   */
  Drupal.theme.addressAutocompleteToggleButton = function (mode, isShown) {
    let label;

    if (isShown) {
      if (mode === "hide") {
        label = Drupal.t("Hide fields");
      }
      else {
        label = Drupal.t("Disable fields");
      }
    }
    else {
      label = Drupal.t("Override fields");
    }

    return "<button class=\"address-autocomplete-toggle-button button\" type=\"button\">" +
      label +
    "</button>";
  };

})(jQuery, Drupal);

